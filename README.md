# Cup of Joe: The Server -- Mega -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/mega/cup_of_joe)

## Chal Info

Desc: `I'm craving some coffee. Maybe you can help me find some?`

Hints:

* What do we actually crave here? How do we get it?

Flag: `TUCTF{d0_y0u_cr4v3_th3_418}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/cup_of_joe)

Ports: 80

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 asciioverflow/cup_of_joe:tuctf2019
```
