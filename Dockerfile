
FROM openjdk:11-jre

WORKDIR /usr/src/app

COPY ./src/ .

CMD ["java", "-XX:+UseContainerSupport", "HttpServer"]
